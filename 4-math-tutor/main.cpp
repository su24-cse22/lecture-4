#include <iostream>
#include <ucm_random>
using namespace std;

int main() {

    /*

        Create a program that asks the user a simple math question.
        The program should randomly decide the mathematical operation
        (addition, subtraction, multiplication, or division) with numbers
        no larger than 20. Division should be treated as integer division.

        Example Input:
        5 + 3 = 7

        Example Output:
        That's incorrect. 5 + 3 = 8

    */

    
    int x, y, op, input, answer;
    string operation;
    RNG generator;

    // generate random number between 1 and 20 for x and y
    x = generator.get(1, 20);
    y = generator.get(1, 20);

    // generate random number between 1 and 4 for op (math operation)
    op = generator.get(1, 4);

    // determine mathematical operation based on value of op
    if (op == 1) {
        operation = "+";
        answer = x + y;
    }
    else if (op == 2) {
        operation = "-";
        answer = x - y;
    }
    else if (op == 3) {
        operation = "*";
        answer = x * y;
    }
    else if (op == 4) {
        operation = "/";
        answer = x / y;
    }

    cout << x << " " << operation << " " << y << " = ";
    cin >> input;

    // determining if user inputted correct answer
    if (input == answer) {
        cout << "That's correct!" << endl;
    } else {
        cout << "That's incorrect. " << x << " " << operation << " " << y << " = " << answer << endl;
    }

    return 0;
}