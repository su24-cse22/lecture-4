#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that creates a shed quote for a 
        customer based on the following specifications:

        Wood Price:    $1.50 sq/ft
        Metal Price:   $2.00 sq/ft
        Roof Price:    $3.00 sq/ft
        Delivery:      $100
        Free Delivery: price of walls and roof is greater than $1500

        Program must take in customer name and address, shed length, width,
        height, and material, and delivery.

        Example Input:
        Rufus Bobcat
        5200 Lake Road
        Merced, CA 95343
        14
        16
        10
        Wood
        1

        Example Output:
        -------------------------
                QUOTE          
        -------------------------
        Rufus Bobcat
        5200 Lake Road
        Merced, CA 95343

        DESCRIPTION SUMMARY
        Dimensions: 14 x 16 x 10
        Material: Wood
        Delivery: Yes

        PRICE SUMMARY
        Walls:    $900
        Roof:     $672
        Delivery: $0
        Total:    $1572

    */

    string name, address1, address2, material;
    float length, width, height;
    bool delivery;  // 1 = true, 0 = false

    getline(cin, name);
    getline(cin, address1);
    getline(cin, address2);
    cin >> length >> width >> height;
    cin.ignore();
    getline(cin, material);
    cin >> delivery;

    float woodPrice = 1.5;
    float metalPrice = 2.0;
    float roofPrice = 3.0;
    float deliveryPrice = 100;

    // determining the price of the material
    float materialPrice = 0;
    if (material == "Wood") {
        materialPrice = woodPrice;
    }
    else if (material == "Metal") {
        materialPrice = metalPrice;
    }

    float wallArea = (2 * length * height) + (2 * width * height);
    float wallCost = wallArea * materialPrice;

    float roofArea = length * width;
    float roofCost = roofArea * roofPrice;

    float deliveryCost = 0;
    
    // determine if customer wants delivery
    if (delivery) {
        // determine if customer qualifies for free delivery
        if (roofCost + wallCost > 1500) {
            deliveryCost = 0;   // customer qualifies for free delivery
        }
        else {
            deliveryCost = deliveryPrice;   // customer does not qualify for free delivery
        }
    } else {
        deliveryCost = 0;   // customer does not want delivery
    }

    float total = wallCost + roofCost + deliveryCost;

    string deliveryString = "No";
    if (delivery) {
        deliveryString = "Yes";
    }

    cout << "-------------------------" << endl;
    cout << "        QUOTE            " << endl;
    cout << "-------------------------" << endl;
    cout << name << endl;
    cout << address1 << endl;
    cout << address2 << endl;
    cout << endl;
    cout << "DESCRIPTION SUMMARY" << endl;
    cout << "Dimensions: " << length << " x " << width << " x " << height << endl;
    cout << "Material: " << material << endl;
    cout << "Delivery: " << deliveryString << endl;
    cout << endl;
    cout << "PRICE SUMMARY" << endl;
    cout << "Walls:    $" << wallCost << endl;
    cout << "Roof:     $" << roofCost << endl;
    cout << "Delivery: $" << deliveryCost << endl;
    cout << "Total:    $" << total << endl;

    return 0;
}