#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that determines if a student passed
        or failed a course. A passing grade is 70 or above.

        Example Input:
        75

        Example Output:
        Pass

    */

    int grade;
    cin >> grade;

    if (grade >= 70) {
        cout << "Pass" << endl;
    } 
    else {
        cout << "Fail" << endl;
    }

    // `else` only runs when the `if` condition is false

    return 0;
}