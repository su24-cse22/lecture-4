#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that determines the letter grade,
        given the following rubric:

        A : [90, 100]
        B : [80 - 90)
        C : [70 - 80)
        D : [60 - 70)
        F : [0, 60)
        Invalid Grade : grade is less than 0 or greater than 100

        Example Input:
        93

        Output:
        A

    */

    float grade;
    cin >> grade;

    /* ------------------------------------------------------------------------- */
    /* Solution 1 */

    // These are 6 separate if statements
    // Each one is checked

    // if (grade >= 90 && grade <= 100) {
    //     cout << "A" << endl;
    // }

    // if (grade >= 80 && grade < 90) {
    //     cout << "B" << endl;
    // }

    // if (grade >= 70 && grade < 80) {
    //     cout << "C" << endl;
    // }

    // if (grade >= 60 && grade < 70) {
    //     cout << "D" << endl;
    // }

    // if (grade >= 0 && grade < 60) {
    //     cout << "F" << endl;
    // }

    // if (grade < 0 || grade > 100) {
    //     cout << "Invalid Grade" << endl;
    // }

    /* ------------------------------------------------------------------------- */

   
   
    /* ------------------------------------------------------------------------- */
    /* Solution 2 */

    // This is only 1 if statement.
    // Only one can be true.
    // Check each condition from top to bottom.
    // Find the first one that evaluates to true, ignore the rest.
    // else condition will ONLY execute when every other condition was false
    
    if (grade > 100) {
        cout << "Invalid Grade" << endl;
    }
    else if (grade >= 90) {
        cout << "A" << endl;
    }
    else if (grade >= 80) {
        cout << "B" << endl;
    }
    else if (grade >= 70) {
        cout << "C" << endl;
    }
    else if (grade >= 60) {
        cout << "D" << endl;
    }
    else if (grade >= 0) {
        cout << "F" << endl;
    } 
    else {
        cout << "Invalid Grade" << endl;
    }

    /* ------------------------------------------------------------------------- */
 
    return 0;
}